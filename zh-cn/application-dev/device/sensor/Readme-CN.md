# Sensor Service Kit（传感器服务）

- [Sensor Service Kit开发简介](sensorservice-kit-intro.md)
- 传感器
  - [传感器开发概述](sensor-overview.md)
  - [传感器开发指导](sensor-guidelines.md)
- 振动
  - [振动开发概述](vibrator-overview.md)
  - [振动开发指导](vibrator-guidelines.md)