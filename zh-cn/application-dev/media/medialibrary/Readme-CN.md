# Media Library Kit（媒体文件管理服务）

- [Media Library Kit 简介](photoAccessHelper-overview.md)
- [开发准备](photoAccessHelper-preparation.md)
- [媒体资源使用指导](photoAccessHelper-resource-guidelines.md)
- [用户相册资源使用指导](photoAccessHelper-userAlbum-guidelines.md)
- [系统相册资源使用指导](photoAccessHelper-systemAlbum-guidelines.md)
- [媒体资源变更通知相关指导](photoAccessHelper-notify-guidelines.md)
