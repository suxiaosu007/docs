# AVSession Kit（音视频播控服务）

- [AVSession Kit简介](avsession-overview.md)
- 本地媒体会话
  - [本地媒体会话概述](local-avsession-overview.md)
  - [媒体会话提供方](using-avsession-developer.md)
  - [应用接入AVSession场景介绍](avsession-access-scene.md)
  - [媒体会话控制方](using-avsession-controller.md)
- 分布式媒体会话
  - [分布式媒体会话概述](distributed-avsession-overview.md)
  - [使用分布式媒体会话](using-distributed-avsession.md)
