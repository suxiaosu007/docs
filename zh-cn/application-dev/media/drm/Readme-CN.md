# DRM Kit（数字版权保护服务）

- [DRM Kit 简介](drm-overview.md)
- 数字版权保护开发指导(ArkTS)
  - [插件管理(ArkTS)](drm-plugin-management.md)
  - [系统管理(ArkTS)](drm-mediakeysystem-management.md)
  - [会话管理(ArkTS)](drm-mediakeysession-management.md)
- 数字版权保护开发指导(C/C++)
  - [系统管理(C/C++)](native-drm-mediakeysystem-management.md)
  - [会话管理(C/C++)](native-drm-mediakeysession-management.md)
