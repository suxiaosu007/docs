# Network Kit（网络服务）

- [Network Kit开发概述](net-mgmt-overview.md)

- Network Kit数据传输能力
    - [HTTP数据请求](http-request.md)
    - [WebSocket连接](websocket-connection.md)
    - [Socket连接](socket-connection.md)
    - [MDNS](net-mdns.md)

- Network Kit网络管理能力
    - [网络连接管理](net-connection-manager.md)
    - [NetConnection开发指导(C/C++)](native-netmanager-guidelines.md)
    - [网络共享](net-sharing.md)
    - [以太网连接管理](net-ethernet.md)
    - [流量管理](net-statistics.md)
    - [VPN管理](net-vpn.md)