# 证书链校验时从p12文件构造TrustAnchor对象数组

证书链校验时从p12文件构造TrustAnchor对象数组。

## 开发步骤

1. 导入[证书算法库框架模块](../../reference/apis-device-certificate-kit/js-apis-cert.md)。

   ```ts
   import cert from '@ohos.security.cert';
   ```

2. 基于现有的p12文件数据，调用[cert.createTrustAnchorsWithKeyStore](../../reference/apis-device-certificate-kit/js-apis-cert.md#cryptocertcreatex509certchain11)创建TrustAnchor数组对象，并返回结果。

  ```ts
  import cert from '@ohos.security.cert';
  import { BusinessError } from '@ohos.base';
  import util from '@ohos.util';

  let p12Data = new Uint8Array([
      0x30,0x82,0x09,0x51,0x02,0x01,0x03,0x30,0x82,0x09,0x17,0x06,0x09,0x2A,0x86,0x48,
      0x86,0xF7,0x0D,0x01,0x07,0x01,0xA0,0x82,0x09,0x08,0x04,0x82,0x09,0x04,0x30,0x82,
      0x09,0x00,0x30,0x82,0x03,0xB7,0x06,0x09,0x2A,0x86,0x48,0x86,0xF7,0x0D,0x01,0x07,
      0x06,0xA0,0x82,0x03,0xA8,0x30,0x82,0x03,0xA4,0x02,0x01,0x00,0x30,0x82,0x03,0x9D,
      0x06,0x09,0x2A,0x86,0x48,0x86,0xF7,0x0D,0x01,0x07,0x01,0x30,0x1C,0x06,0x0A,0x2A,
      0x86,0x48,0x86,0xF7,0x0D,0x01,0x0C,0x01,0x06,0x30,0x0E,0x04,0x08,0xDB,0x74,0x8E,
      0x33,0xBF,0x64,0x2B,0xFA,0x02,0x02,0x08,0x00,0x80,0x82,0x03,0x70,0xF8,0x7A,0x13,
      0xA7,0x9C,0x08,0x3E,0x20,0x78,0x7F,0x33,0x6E,0x95,0x09,0xBF,0x69,0x62,0xC1,0x0B,
      0x7D,0x72,0x05,0x8F,0x9C,0xB6,0xA7,0x80,0x7C,0x47,0x06,0xB1,0x85,0x1C,0xAE,0x98,
      0x06,0x61,0x56,0xDF,0x2D,0xB7,0x57,0xE2,0xF8,0xA0,0x8F,0xD5,0xB8,0x78,0xEB,0x5C,
      0x0B,0xA8,0xD2,0x37,0xE1,0x07,0x42,0x85,0x5A,0xB6,0x72,0x43,0x68,0xCD,0x0D,0xBC,
      0x18,0x28,0xA1,0x47,0xF3,0x92,0xF2,0x98,0x62,0x8B,0x70,0x99,0xAA,0x73,0x03,0x60,
      0x6D,0x7C,0x90,0x6D,0xEE,0x1B,0xFA,0xED,0x85,0x2F,0x95,0x72,0x81,0x09,0x35,0x33,
      0x09,0x36,0x55,0x7F,0xB9,0x1D,0xDA,0x32,0x8F,0x31,0xDD,0x2D,0x31,0x43,0x15,0x49,
      0x68,0x6B,0x9E,0x10,0x49,0x75,0xDB,0x11,0xBA,0x4F,0x37,0x89,0x02,0xB2,0x8D,0x52,
      0xF6,0x95,0x93,0x80,0xD8,0x02,0xFB,0x42,0x9F,0x51,0x58,0xCF,0x09,0xB5,0xB6,0x9A,
      0x8A,0x19,0xA3,0x58,0x2B,0x96,0x18,0x7B,0xA5,0xEC,0x36,0x6D,0x51,0xA4,0x00,0xD5,
      0xE8,0xEF,0x5B,0x38,0xB4,0x5B,0x58,0x21,0x56,0x1C,0xDD,0x5D,0xF4,0x7F,0x53,0xB9,
      0x55,0xFF,0xED,0x0D,0xA6,0x23,0x4E,0xC4,0x9D,0x1A,0x10,0xCF,0x4A,0x2D,0xF3,0x5E,
      0xA1,0x55,0x1F,0xB4,0xBA,0x5C,0xE0,0x88,0x8A,0x7C,0x42,0x9D,0x0F,0x56,0x63,0xFB,
      0x41,0x7B,0x7E,0x6F,0x30,0xFD,0x92,0x9D,0x32,0xBE,0x29,0x43,0x26,0xAD,0x6E,0x62,
      0xA4,0xCE,0xEC,0x29,0x2F,0x12,0x96,0x1F,0x54,0x80,0xCE,0xDB,0xFF,0x33,0x26,0x68,
      0x05,0xEE,0x64,0xD6,0x90,0xA2,0x3F,0xF0,0x5E,0xCC,0xEA,0x91,0xCF,0x63,0xC2,0x93,
      0x04,0xF7,0x0B,0x3C,0x06,0x03,0xE9,0x11,0xBD,0x35,0x69,0x3E,0x43,0x6B,0x9F,0x26,
      0x13,0x48,0x25,0xF5,0x73,0x86,0x32,0x18,0xFC,0x75,0x5A,0x72,0xAD,0xBA,0x88,0x26,
      0x7C,0xCA,0x6C,0x5D,0xF7,0x22,0x2C,0x7A,0x24,0xFC,0x3E,0x80,0x58,0xE0,0xD9,0x6C,
      0x97,0xE9,0xA1,0x7A,0x76,0x8F,0x2C,0xA4,0xD1,0x9B,0xB8,0xCC,0x36,0xB8,0x93,0x11,
      0x60,0x60,0xE2,0x46,0x0A,0x71,0x50,0xAB,0xB7,0xF4,0xDF,0x2B,0x30,0xC9,0x25,0xCE,
      0x74,0x6D,0xFE,0x22,0x3E,0xAB,0x1B,0xEC,0x70,0xA3,0xA4,0x31,0x8D,0x6D,0x66,0xCF,
      0x4A,0x6C,0x07,0xA8,0x23,0xDA,0xB9,0x07,0x67,0xFE,0xEF,0xF7,0xA9,0xA5,0xCB,0x39,
      0xC1,0x2A,0xC8,0x5D,0x6E,0x9D,0xD8,0xA8,0xA3,0xB9,0x5F,0x00,0xF8,0x25,0xBD,0xA6,
      0x79,0xC3,0xC9,0x9E,0x5A,0xC8,0x22,0x4A,0xF3,0xAF,0x0D,0xC2,0x6F,0xD7,0x59,0x3A,
      0x62,0x82,0x8B,0x5E,0x5D,0xFA,0xFC,0xC5,0x9B,0x5C,0x8C,0x46,0x36,0x3A,0xBC,0xC3,
      0x3F,0xF1,0xCA,0x84,0x15,0x60,0x2C,0xE5,0xA3,0xDE,0xE7,0x24,0x74,0xBA,0xEE,0xCF,
      0xF6,0x9B,0xC2,0x77,0x2A,0x6E,0x1B,0x4E,0x0D,0x9F,0x14,0x85,0x83,0xB0,0x86,0x8A,
      0x95,0x08,0xEA,0x8F,0x8C,0xA8,0xA8,0xBE,0x37,0x54,0xA1,0x46,0x4D,0xD5,0x15,0x7C,
      0xB8,0xDB,0xAC,0x65,0x5E,0x20,0x15,0x8A,0x54,0x4F,0xBF,0x8E,0x22,0x96,0x3B,0xF7,
      0xBE,0x2F,0x47,0xA5,0x53,0x56,0x6C,0xBC,0xCF,0x36,0xDC,0x6B,0x08,0x2A,0x4D,0x9F,
      0x24,0x48,0x31,0x18,0xE8,0x50,0x53,0x6A,0xB9,0x9E,0x1D,0x87,0x82,0x90,0xA1,0xF7,
      0x1F,0x68,0x5A,0x9E,0x7B,0x89,0xC0,0x5F,0x12,0x19,0xFE,0xE2,0x53,0xC0,0x4A,0x6A,
      0x50,0x63,0x85,0x85,0xBE,0x49,0x45,0xFA,0x23,0x14,0x01,0xB9,0x76,0xDA,0x8C,0xBD,
      0xFB,0x24,0x37,0x64,0xBE,0x59,0x78,0x19,0x98,0x7C,0x6E,0xBE,0x47,0x63,0x39,0xC2,
      0x98,0xE6,0x33,0xB4,0x50,0xA7,0xCC,0x69,0xF3,0x08,0x19,0x20,0x7B,0x85,0x9C,0xAF,
      0x3C,0x59,0xCE,0xD9,0xE2,0x53,0xE8,0xA0,0x47,0xB9,0xFF,0xC8,0x1B,0x2D,0xE0,0xFC,
      0x35,0x52,0x74,0x3C,0xC5,0xAD,0x82,0xF7,0x18,0xF8,0xC7,0xD0,0x80,0x0D,0x5E,0xE8,
      0x97,0x64,0x47,0x08,0x07,0x59,0x7A,0x9F,0x04,0x37,0x32,0x20,0xDE,0x3B,0xC7,0xE5,
      0xE8,0x17,0x08,0xBD,0xDA,0x5E,0x3D,0xC5,0x16,0x09,0xA4,0x13,0x25,0xB6,0xCE,0x01,
      0xAD,0x30,0x66,0x4C,0xC6,0x7A,0xF5,0x96,0xBA,0x76,0x74,0xBB,0x54,0x7C,0x02,0xB3,
      0x30,0x28,0xD8,0xA8,0xCD,0x5E,0x04,0xF4,0xC4,0x8F,0xA5,0x6E,0x83,0xE3,0x0F,0xA5,
      0x14,0x49,0x9F,0x0B,0x0C,0xAF,0x33,0x04,0x36,0x37,0xD6,0xCE,0xFC,0x08,0xE4,0x9A,
      0x59,0x0D,0x54,0x66,0x44,0xAD,0x12,0x47,0x35,0x84,0x43,0x28,0xDA,0x66,0x8A,0x0B,
      0x44,0x8F,0x3C,0x2E,0x04,0x26,0x4E,0x7C,0xB8,0xDF,0x03,0xEC,0x1A,0xB6,0x5E,0xEA,
      0x6B,0xE3,0x4D,0xDF,0xD4,0xB0,0xFD,0x95,0x3B,0x46,0x56,0x20,0x26,0x59,0xAE,0x5E,
      0x95,0x20,0x41,0xE7,0xD7,0xD4,0xE5,0xAF,0xFF,0xE9,0x32,0x59,0x32,0xFD,0x72,0xE7,
      0x6D,0x22,0x9F,0x13,0x85,0xDD,0x29,0xBD,0xE2,0x99,0xAE,0xF9,0x51,0x27,0x41,0x0B,
      0x84,0xAB,0x66,0x51,0x1A,0x07,0x9D,0x0B,0x23,0x5F,0xE4,0x74,0xAB,0x43,0x76,0xF1,
      0x01,0xE8,0x34,0x34,0xFC,0x3D,0xD1,0xE5,0x04,0x79,0xE3,0x83,0x6F,0xB0,0x58,0x29,
      0x4F,0xF8,0x3A,0xBF,0x88,0x18,0x41,0x20,0xFF,0x75,0x1F,0xD2,0x86,0xB7,0xFD,0xD2,
      0xDC,0xE6,0x74,0x41,0xF3,0x33,0xC7,0x88,0x7A,0x0F,0x1A,0x19,0x43,0xAB,0x23,0xC4,
      0x9D,0x62,0xF8,0x6C,0x55,0x90,0x7F,0x4C,0xEC,0x80,0xB4,0x1E,0x2A,0x7E,0xB9,0x7F,
      0xD9,0xF3,0x21,0x50,0x9D,0x34,0xFD,0xA2,0x01,0xCA,0xC7,0xA7,0x1F,0x7A,0x86,0x07,
      0x9D,0x3C,0xF7,0x21,0xFD,0x35,0xE4,0x7F,0x7F,0x53,0x93,0x7E,0x52,0x30,0x82,0x05,
      0x41,0x06,0x09,0x2A,0x86,0x48,0x86,0xF7,0x0D,0x01,0x07,0x01,0xA0,0x82,0x05,0x32,
      0x04,0x82,0x05,0x2E,0x30,0x82,0x05,0x2A,0x30,0x82,0x05,0x26,0x06,0x0B,0x2A,0x86,
      0x48,0x86,0xF7,0x0D,0x01,0x0C,0x0A,0x01,0x02,0xA0,0x82,0x04,0xEE,0x30,0x82,0x04,
      0xEA,0x30,0x1C,0x06,0x0A,0x2A,0x86,0x48,0x86,0xF7,0x0D,0x01,0x0C,0x01,0x03,0x30,
      0x0E,0x04,0x08,0x4B,0x0A,0xF5,0x88,0x4D,0xF0,0xC3,0x73,0x02,0x02,0x08,0x00,0x04,
      0x82,0x04,0xC8,0xF0,0xAF,0x17,0xCC,0x58,0x9A,0xCC,0xEA,0x5E,0x66,0x83,0xC3,0xB3,
      0x05,0xA3,0xE4,0xAE,0x77,0x9A,0x97,0xCE,0x8E,0x63,0x66,0x8F,0x08,0x02,0x95,0x8D,
      0x1B,0xD7,0x5B,0x44,0x5B,0xAD,0x3F,0xA9,0x77,0x4B,0x76,0x4A,0x69,0x87,0x18,0xA6,
      0x04,0x43,0xE1,0xAF,0x1E,0x68,0xF4,0x6B,0x87,0x90,0xB4,0x9A,0x7A,0x8B,0x95,0xB8,
      0x98,0x6D,0x94,0x3B,0xD5,0xF3,0x66,0x3B,0x10,0x2E,0xFF,0x23,0xEC,0x05,0xD5,0xED,
      0x12,0xCE,0x71,0xC0,0x86,0xF8,0x22,0x77,0xCC,0xE7,0xB8,0x39,0x1C,0x46,0x70,0x37,
      0x5B,0x61,0xAA,0xAB,0xDF,0xB7,0x6D,0xD7,0x6C,0xE6,0x58,0xCC,0x1B,0xE4,0x52,0x8E,
      0x83,0x02,0x97,0x81,0x39,0x93,0x7D,0xDD,0x6A,0x01,0x77,0xCB,0xBE,0xE7,0xF5,0xB8,
      0xA3,0x69,0xD7,0x7F,0x80,0xEE,0x48,0x7C,0x16,0xF2,0xED,0x9C,0x73,0x1A,0x93,0x6B,
      0x46,0x3D,0xDD,0x39,0xC4,0x55,0xC1,0x37,0xF0,0xE3,0x1A,0xCC,0x9A,0xA3,0x2B,0xCD,
      0x35,0xAB,0x85,0xF9,0xC2,0x90,0x00,0x85,0xF3,0x21,0x10,0x4C,0xC8,0x26,0x99,0x53,
      0x26,0xED,0x6F,0x10,0xA0,0xFC,0x29,0xFB,0xF3,0x0F,0xFB,0xE6,0x82,0xC2,0xA2,0x6B,
      0x3E,0x5F,0x96,0xA3,0x91,0x56,0x69,0x27,0x44,0x48,0x77,0xD3,0x25,0xEE,0x59,0xD4,
      0x22,0x50,0xDB,0xAD,0x18,0xDC,0x52,0xBF,0xCA,0x9F,0x4E,0xCB,0xC1,0xC0,0x3D,0x8D,
      0xBB,0x50,0xCD,0x66,0x92,0x12,0x63,0x88,0xC6,0xA2,0xD5,0x82,0xAE,0xCF,0xB6,0xAA,
      0xD8,0x05,0x47,0xD7,0xCF,0x63,0x22,0xD5,0xFD,0x45,0xB1,0x1A,0x3A,0x2D,0x83,0x06,
      0xCA,0x71,0x42,0xE6,0x7E,0x73,0x82,0x42,0x4C,0x52,0x55,0x69,0x42,0xFE,0x52,0xC9,
      0xD1,0xAF,0xB3,0xA2,0x4E,0x23,0x8D,0x50,0x36,0xFF,0x59,0x9F,0xBD,0x9F,0xF8,0x75,
      0x0D,0x00,0xFC,0x49,0x8B,0x7A,0x9D,0x92,0x9C,0xA4,0x26,0x31,0x6D,0x6D,0x5B,0x36,
      0x38,0xBA,0xD7,0x8D,0x79,0xB7,0x53,0x7D,0xBB,0x57,0x87,0xBA,0x93,0x2E,0x21,0x09,
      0xB5,0x31,0xE4,0x24,0xF7,0xA6,0x78,0xB0,0xD5,0xB4,0x2E,0x0E,0x1E,0x22,0xF3,0x3B,
      0xC6,0x2F,0x8B,0xCA,0x05,0xFE,0xBE,0xFE,0x24,0x05,0xE0,0x68,0xA8,0x07,0x1E,0x1C,
      0x55,0x24,0x7B,0x53,0x23,0x38,0x85,0x3A,0x42,0xF3,0xF8,0x4D,0x30,0x13,0xDD,0x29,
      0xB5,0x41,0x98,0xBC,0x77,0x64,0x75,0x38,0x35,0xFE,0x5C,0x7C,0x56,0x10,0xBA,0x6A,
      0xCD,0x01,0x04,0x3C,0x94,0x5A,0x98,0xE9,0xF4,0x70,0x99,0xA3,0x76,0xF2,0x31,0x6D,
      0x78,0x58,0x59,0x54,0xAC,0x5A,0x6E,0x54,0xB7,0x8D,0x78,0x18,0x68,0x5A,0x1F,0xE0,
      0x2E,0x4F,0x19,0x2F,0x82,0xE6,0x84,0xF2,0xCA,0x46,0x9A,0x5C,0x6E,0x5A,0xEE,0x61,
      0xBE,0xC3,0x09,0xFC,0xD9,0x7A,0x07,0xB3,0x79,0x06,0x29,0xE1,0xC1,0x47,0xA0,0x39,
      0x36,0x25,0x59,0xE4,0x57,0x5A,0xF4,0x63,0x39,0xDB,0x5C,0x5A,0x22,0x26,0xF4,0x71,
      0x4C,0xAA,0x4E,0xAA,0xA7,0x4D,0xA0,0xD4,0xFC,0xD2,0xEA,0xDE,0xCF,0x78,0xAA,0x20,
      0x23,0xEE,0xEB,0xCF,0x8A,0x6B,0xE1,0x67,0x39,0xCB,0xCA,0xEF,0xF2,0xDB,0x84,0xA6,
      0x37,0x99,0x2E,0xEC,0xC7,0xF3,0xAD,0x4E,0xBE,0xB0,0x31,0x8D,0x7A,0x09,0xAB,0x96,
      0x66,0xDC,0xC0,0xA1,0x2A,0x35,0x5D,0x7A,0x8E,0xA6,0xB3,0x94,0x31,0x93,0xB8,0x5C,
      0x06,0x05,0x8C,0x53,0x2F,0x8B,0x83,0xAD,0x3B,0xE9,0x64,0x5F,0xE5,0xD9,0x02,0x17,
      0xD1,0x86,0x22,0x9C,0x3C,0xC4,0x95,0xFA,0x95,0x43,0xD6,0xC7,0x66,0x9B,0xBE,0x89,
      0x48,0x39,0xB0,0xBE,0x27,0xC1,0xDF,0x96,0xC0,0x6B,0x06,0x61,0x21,0x3A,0x48,0x5C,
      0x0C,0x66,0x1D,0x6A,0x6B,0x63,0x76,0xE6,0xAB,0x47,0x69,0xDD,0x1B,0x96,0x24,0xAF,
      0xCA,0x1A,0xDA,0x10,0x9B,0xC2,0xC1,0x43,0xAA,0xDC,0x60,0x48,0xF5,0x43,0xA9,0xA2,
      0x59,0x1C,0x20,0xA3,0xBC,0x1E,0x40,0x67,0x76,0xAB,0x77,0xFB,0x4C,0xC8,0x33,0xDD,
      0xF8,0x8E,0xED,0xF0,0xC6,0xDF,0xAE,0x38,0xA9,0xB5,0xB9,0xF6,0x87,0xBF,0xF8,0x1A,
      0x1F,0x4A,0x67,0x1D,0x26,0xA0,0x17,0xFD,0xD6,0xA2,0x79,0x99,0x19,0xD0,0x0F,0x34,
      0x4B,0x80,0x9E,0xED,0x92,0x03,0xC5,0x1D,0x52,0xAA,0x82,0x90,0xBD,0xFE,0x39,0x45,
      0x52,0x2E,0x4E,0x6C,0x74,0x2E,0x74,0x91,0x7D,0x67,0x40,0x0F,0x08,0x13,0x78,0x6A,
      0xA7,0xF9,0x40,0xFF,0xB9,0xD9,0x09,0xD2,0xFF,0x16,0x44,0xA7,0x57,0xA2,0x11,0x7C,
      0x39,0x5F,0xA2,0xF8,0x72,0xA2,0x7A,0x8B,0x86,0x59,0xAB,0xFA,0xB3,0x73,0x86,0xD7,
      0xA8,0xEB,0x8E,0x52,0xDE,0x32,0x63,0x8A,0x4B,0xBF,0x2A,0x03,0x98,0x1E,0xB6,0x26,
      0x05,0xC7,0xA1,0xD1,0x0A,0xA2,0xA3,0x5C,0x2D,0x9D,0x50,0xFD,0xAC,0x30,0x6E,0xB3,
      0x3D,0x33,0xD0,0x93,0x4D,0x5B,0xE8,0x63,0x71,0x69,0x04,0x80,0xAD,0x06,0x18,0x75,
      0x6E,0x33,0x26,0x4A,0xA5,0xDB,0xD8,0x8C,0x1E,0x99,0x95,0x20,0x6A,0xB1,0x8E,0x64,
      0x5D,0xBF,0xF3,0xC3,0x1E,0x77,0xFD,0x44,0xD2,0xD1,0x51,0x0F,0x13,0x0A,0xD2,0x66,
      0x2E,0xB8,0x4B,0xC8,0x37,0x4F,0x83,0x6D,0x17,0x52,0xA4,0x16,0xF8,0x29,0x03,0xF9,
      0x99,0xA2,0x31,0xD0,0x9A,0x55,0xF5,0x32,0x63,0x2F,0x94,0x76,0x31,0x90,0x5E,0x4E,
      0xB1,0x13,0x8E,0x5C,0xA1,0x8A,0xB1,0x7C,0x09,0x0A,0xC0,0xC8,0xCA,0x15,0x94,0x42,
      0x18,0xB4,0xD1,0xF2,0x08,0x48,0x69,0xA8,0x3E,0xCD,0x41,0x4C,0x97,0xB9,0x07,0x07,
      0xDF,0xB9,0x12,0x08,0xE8,0xF0,0xF2,0xAF,0x29,0x2A,0x27,0x31,0x7E,0xF8,0xDE,0x03,
      0x8B,0x67,0xFA,0x96,0xAA,0xFF,0xAB,0xA6,0x41,0x12,0x9F,0x56,0xD2,0x4E,0x5D,0xDF,
      0x46,0xE1,0xEE,0x2D,0xB2,0x45,0x48,0xCC,0x60,0x97,0xE1,0x9F,0x65,0x18,0xFA,0x36,
      0x39,0x91,0x03,0x3F,0xEE,0x30,0x35,0xD5,0x64,0xE2,0x00,0xDF,0xD6,0x18,0x3D,0x38,
      0xBA,0x4D,0x64,0xD9,0x67,0x5F,0x07,0x9C,0xFC,0x6F,0x10,0x73,0x91,0x8B,0xFB,0x38,
      0x7E,0xF1,0x10,0x01,0x8E,0x0F,0xF9,0x9A,0xE4,0x66,0xFA,0x39,0x9B,0xCB,0xC8,0x12,
      0x30,0x35,0x2E,0x08,0x5E,0x3F,0xD1,0x59,0x03,0x51,0x1C,0x4F,0x0A,0x17,0x2C,0x62,
      0x53,0x40,0xAA,0xD0,0x1F,0xEC,0x43,0x5B,0x69,0x6D,0x94,0xB7,0x32,0x7C,0xCF,0x93,
      0xA7,0xE0,0xE1,0x8C,0xCB,0xD3,0x1F,0x18,0x74,0x30,0x1B,0x09,0xA5,0x52,0xA4,0xD4,
      0xC1,0x56,0x9C,0x3C,0x32,0x44,0x82,0x9F,0x4D,0xB8,0x26,0xA3,0x84,0x86,0x71,0x8B,
      0xCD,0xBF,0xA6,0x78,0x84,0xDD,0xDC,0x43,0xAA,0x45,0x70,0xD4,0x8C,0x11,0x2F,0xB6,
      0x51,0x4F,0xD4,0xD7,0x86,0x72,0xB8,0x2C,0x63,0x69,0x31,0x76,0xBF,0x0C,0x66,0xA8,
      0xD8,0x0C,0xE7,0x30,0xE8,0x6E,0xBE,0x84,0xFB,0xA6,0x78,0x85,0x6D,0xFA,0x32,0xFF,
      0x24,0x44,0x51,0x4B,0x6A,0x67,0x44,0x14,0xE6,0x7A,0x20,0x12,0x46,0x9A,0xA2,0x60,
      0x88,0x0A,0xF9,0xFC,0xF4,0xC3,0xC9,0x30,0x82,0x6D,0x5B,0xC8,0x41,0xDD,0x74,0xA3,
      0x85,0xCB,0x20,0x63,0xD5,0x6E,0xB5,0x60,0x82,0x1D,0x4B,0x6F,0x70,0xFB,0x68,0x5B,
      0xFD,0x9D,0x88,0xA9,0x33,0x1D,0x1F,0xAD,0xE0,0xAF,0x08,0x0F,0x15,0x1F,0x0A,0xB4,
      0x4D,0x0B,0x5B,0x6C,0x09,0xD6,0x66,0x5F,0x44,0xDC,0x27,0x11,0x1C,0x5E,0x08,0x8F,
      0x87,0xB9,0x4C,0x92,0xFC,0x9F,0x92,0x9F,0xC9,0x2E,0x6D,0x0E,0x8D,0x18,0x40,0x4B,
      0xE8,0xFA,0xCC,0x1A,0xED,0xFF,0xD2,0x32,0xA9,0x86,0x0C,0x21,0xDD,0x96,0x46,0x6F,
      0x91,0x30,0x30,0xFC,0x86,0x4B,0x6C,0x07,0x67,0x15,0x42,0x0A,0x15,0xCC,0x52,0xD1,
      0xFB,0x35,0xDD,0x2A,0xBC,0x1D,0xBB,0x77,0x1D,0x63,0xFB,0x38,0xF4,0x5B,0x8D,0x53,
      0xA6,0xD8,0xCB,0x65,0x3C,0xF5,0xB9,0xEF,0x40,0x6B,0xFF,0x31,0x25,0x30,0x23,0x06,
      0x09,0x2A,0x86,0x48,0x86,0xF7,0x0D,0x01,0x09,0x15,0x31,0x16,0x04,0x14,0x52,0xAC,
      0x26,0xBF,0x83,0x1F,0xB3,0xDE,0x0C,0x3B,0x5E,0x75,0xE1,0x19,0x31,0xAF,0x29,0x17,
      0x5F,0x3F,0x30,0x31,0x30,0x21,0x30,0x09,0x06,0x05,0x2B,0x0E,0x03,0x02,0x1A,0x05,
      0x00,0x04,0x14,0x02,0x50,0xE6,0x4F,0xD8,0x0F,0x8D,0x9A,0x3C,0x5A,0xFC,0x7F,0x86,
      0xE8,0xF0,0x7C,0x4F,0x95,0x87,0xE0,0x04,0x08,0x02,0x18,0x7D,0x28,0xFD,0x49,0xF6,
      0xBB,0x02,0x02,0x08,0x00]);

  try {
    cert.createTrustAnchorsWithKeyStore(p12Data, '123456').then((data) => {
      console.log('createTrustAnchorsWithKeyStore sucess, the num of result is :' + JSON.stringify(data.length))
    }).cache((err) => {
      console.error('createTrustAnchorsWithKeyStore failed, error :' + JSON.stringify(err))
    })
  } catch(error) {
    console.error('createTrustAnchorsWithKeyStore failed, error :' + JSON.stringify(error))
  }
  ```
