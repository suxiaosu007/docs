# Background Tasks Kit（后台任务开发服务）

- 后台任务管理
  - [后台任务总体概述](background-task-overview.md)
  - [短时任务](transient-task.md)
  - [长时任务](continuous-task.md)
  - [延迟任务](work-scheduler.md)
  - [代理提醒](agent-powered-reminder.md)
  - [能效资源申请（仅对系统特权应用开放）](efficiency-resource-request.md)
- [设备使用信息统计（仅对系统应用开放）](../device-usage-statistics/Readme-CN.md)