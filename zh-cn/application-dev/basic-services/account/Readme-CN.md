# 帐号管理

- [帐号管理概述](account-overview.md)
- 系统帐号
  - [管理系统帐号（仅对系统应用开放）](manage-os-account.md)
  - [使用约束管控系统帐号](control-os-account-by-constraints.md)
  - [管理系统帐号凭据（仅对系统应用开放）](manage-os-account-credential.md)
- 域帐号（仅对系统应用开放）
  - [管理域帐号](manage-domain-account.md)
  - [认证域帐号](auth-domain-account.md)
  - [管理域帐号插件](manage-domain-plugin.md)
- 分布式帐号（仅对系统应用开放）
  - [管理分布式帐号](manage-distributed-account.md)
- 应用帐号
  - [管理应用帐号](manage-application-account.md)
