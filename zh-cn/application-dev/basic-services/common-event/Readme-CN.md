# 应用事件

- 进程间通信
    - [公共事件简介](common-event-overview.md)
    - 公共事件订阅
        - [公共事件订阅概述](common-event-subscription-overview.md)
        - [动态订阅公共事件](common-event-subscription.md)
        - [静态订阅公共事件（仅对系统应用开放）](common-event-static-subscription.md)
        - [取消动态订阅公共事件](common-event-unsubscription.md)
    - [公共事件发布](common-event-publish.md)
    - [移除粘性公共事件（仅对系统应用开放）](common-event-remove-sticky.md)
- 线程间通信
    - [使用Emitter进行线程间通信](itc-with-emitter.md)
