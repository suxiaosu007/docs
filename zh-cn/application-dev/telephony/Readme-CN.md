# Telephony Kit（蜂窝通信服务）

- [电话服务开发概述](telephony-overview.md)
- [拨打电话](telephony-call.md)
- [短信服务](telephony-sms.md)
