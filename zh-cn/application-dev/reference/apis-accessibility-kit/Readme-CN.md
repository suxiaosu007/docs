# Accessibility Kit API参考

- ArkTS API
  - [@ohos.accessibility (辅助功能)](js-apis-accessibility.md)
  - [@ohos.accessibility.GesturePath (手势路径)](js-apis-accessibility-GesturePath.md)
  - [@ohos.accessibility.GesturePoint (手势触摸点)](js-apis-accessibility-GesturePoint.md)
  - [@ohos.application.AccessibilityExtensionAbility (辅助功能扩展能力)](js-apis-application-accessibilityExtensionAbility.md)
  - [AccessibilityExtensionContext (辅助功能扩展上下文)](js-apis-inner-application-accessibilityExtensionContext.md)
  - [@ohos.accessibility.config (系统辅助功能配置)(系统接口)](js-apis-accessibility-config-sys.md)
- 错误码
  - [无障碍子系统错误码](errorcode-accessibility.md)
