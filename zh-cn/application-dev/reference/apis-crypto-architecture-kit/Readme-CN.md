# Crypto Architecture Kit API参考

- ArkTS API
  - [@ohos.security.cryptoFramework (加解密算法库框架)](js-apis-cryptoFramework.md)
  - 已停止维护的接口
    - [@system.cipher (加密算法)](js-apis-system-cipher.md)
- 错误码
  - [crypto framework错误码](errorcode-crypto-framework.md)
