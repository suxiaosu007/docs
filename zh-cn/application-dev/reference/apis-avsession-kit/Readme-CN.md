# AVSession Kit API参考

- ArkTS API
  - [@ohos.multimedia.avsession (媒体会话管理)](js-apis-avsession.md)
  - [@ohos.multimedia.avCastPickerParam (投播组件参数)](js-apis-avCastPickerParam.md)
  - [@ohos.app.ability.MediaControlExtensionAbility (播控扩展能力)(系统接口)](js-apis-app-ability-MediaControlExtensionAbility-sys.md)
  - [@ohos.multimedia.avsession (媒体会话管理)(系统接口)](js-apis-avsession-sys.md)
  - application
    - [MediaControlExtensionContext (播控扩展能力上下文)(系统接口)](js-apis-inner-application-MediaControlExtensionContext-sys.md)
- ArkTS组件
  - [@ohos.multimedia.avCastPicker (投播组件)](ohos-multimedia-avcastpicker.md)
- 错误码
  - [媒体会话管理错误码](errorcode-avsession.md)
