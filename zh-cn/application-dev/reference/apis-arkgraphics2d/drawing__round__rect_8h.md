# drawing_round_rect.h


## 概述

文件中定义了与圆角矩形相关的功能函数。

**引用文件：**&lt;native_drawing/drawing_round_rect.h&gt;

**库：** libnative_drawing.so

**起始版本：** 11

**相关模块：**[Drawing](_drawing.md)


## 汇总


### 函数

| 名称 | 描述 |
| -------- | -------- |
| [OH_Drawing_RoundRect](_drawing.md#oh_drawing_roundrect) \* [OH_Drawing_RoundRectCreate](_drawing.md#oh_drawing_roundrectcreate) (const [OH_Drawing_Rect](_drawing.md#oh_drawing_rect) \*, float xRad, float yRad) | 用于创建一个圆角矩形对象。 |
| void [OH_Drawing_RoundRectDestroy](_drawing.md#oh_drawing_roundrectdestroy) ([OH_Drawing_RoundRect](_drawing.md#oh_drawing_roundrect) \*) | 用于销毁圆角矩形对象并回收该对象占有的内存。 |
