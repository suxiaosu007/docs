# Location Kit API参考

- ArkTS API
  - [@ohos.geoLocationManager (位置服务)](js-apis-geoLocationManager.md)
  - [@ohos.geoLocationManager (位置服务)(系统接口)](js-apis-geoLocationManager-sys.md)
  - 已停止维护的接口
    - [@ohos.geolocation (位置服务)](js-apis-geolocation.md)
    - [@system.geolocation (地理位置)](js-apis-system-location.md)
- 错误码
  - [位置服务错误码](errorcode-geoLocationManager.md)
