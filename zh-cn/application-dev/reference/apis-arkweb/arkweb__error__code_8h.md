# arkweb_error_code.h


## 概述

声明ArkWeb NDK接口异常错误码。

**库：** libohweb.so

**系统能力：** SystemCapability.Web.Webview.Core

**起始版本：** 12

**相关模块：**[Web](_web.md)


## 汇总


### 类型定义

| 名称 | 描述 | 
| -------- | -------- |
| typedef enum [ArkWeb_ErrorCode](_web.md#arkweb_errorcode) [ArkWeb_ErrorCode](_web.md#arkweb_errorcode) | 定义ArkWeb NDK接口异常错误码。  | 


### 枚举

| 名称 | 描述 | 
| -------- | -------- |
| [ArkWeb_ErrorCode](_web.md#arkweb_errorcode) { [ARKWEB_ERROR_UNKNOWN](_web.md) = 17100100, [ARKWEB_INVALID_PARAM](_web.md) = 17100101, [ARKWEB_SCHEME_REGISTER_FAILED](_web.md) = 17100102 } | 定义ArkWeb NDK接口异常错误码。  | 
