# User Authentication Kit API参考

- ArkTS API
  - [@ohos.userIAM.userAuth (用户认证)](js-apis-useriam-userauth.md)
  - [@ohos.userIAM.faceAuth (人脸认证)(系统接口)](js-apis-useriam-faceauth-sys.md)
  - [@ohos.userIAM.userAuth (用户认证)(系统接口)](js-apis-useriam-userauth-sys.md)
- 错误码
  - [用户认证错误码](errorcode-useriam.md)
