# Data Loss Prevention Kit API参考

- ArkTS API
  - [@ohos.dlpPermission (数据防泄漏)](js-apis-dlppermission.md)
  - [@ohos.dlpPermission (数据防泄漏)(系统接口)](js-apis-dlppermission-sys.md)
- 错误码
  - [DLP服务错误码](errorcode-dlp.md)
