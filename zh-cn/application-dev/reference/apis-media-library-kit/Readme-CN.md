# Media Library Kit API参考

- ArkTS API
  - [@ohos.file.photoAccessHelper (相册管理模块)](js-apis-photoAccessHelper.md)
  - [@ohos.file.photoAccessHelper (相册管理模块)(系统接口)](js-apis-photoAccessHelper-sys.md)
  - 已停止维护的接口
    - [@ohos.multimedia.medialibrary (媒体库管理)](js-apis-medialibrary.md)
    - [@ohos.multimedia.medialibrary (媒体库管理)(系统接口)](js-apis-medialibrary-sys.md)
