# net_ssl_c.h


## 概述

为SSL/TLS证书链校验模块定义C接口。

**库：** libnet_ssl.so

**系统能力：** SystemCapability.Communication.NetStack

**起始版本：** 11

**相关模块：**[Netstack](netstack.md)


## 汇总


### 函数

| 名称 | 描述 | 
| -------- | -------- |
| [OH_NetStack_VerifyCertification](netstack.md#oh_netstack_verifycertification) (const struct [NetStack_CertBlob](_net_stack___cert_blob.md) \*cert, const struct [NetStack_CertBlob](_net_stack___cert_blob.md) \*caCert) | 证书链校验接口。 | 
