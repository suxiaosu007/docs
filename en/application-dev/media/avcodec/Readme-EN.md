# AVCodec Kit

- [Introduction to AVCodec Kit](avcodec-kit-intro.md)
- Audio and Video Codecs
  - [Obtaining Supported Codecs](obtain-supported-codecs.md)
  - [Audio Encoding](audio-encoding.md)
  - [Audio Decoding](audio-decoding.md)
  - [Video Encoding](video-encoding.md)
  - [Video Decoding](video-decoding.md)
- File Muxing and Demuxing
  - [Audio and Video Muxing](audio-video-muxer.md)
  - [Audio and Video Demuxing](audio-video-demuxer.md)
