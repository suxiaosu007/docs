# Media Library Kit (Media File Management Service)

- [Introduction to Media Library Kit](photoAccessHelper-overview.md)
- [Before You Start](photoAccessHelper-preparation.md)
- [Managing Media Assets](photoAccessHelper-resource-guidelines.md)
- [Managing User Albums](photoAccessHelper-userAlbum-guidelines.md)
- [Managing System Albums](photoAccessHelper-systemAlbum-guidelines.md)
- [Observing Media Assets](photoAccessHelper-notify-guidelines.md)
