# DRM Kit

- [Introduction to DRM Kit](drm-overview.md)
- DRM Development (ArkTS)
  - [DRM Plug-in Management (ArkTS)](drm-plugin-management.md)
  - [Media Key System Management (ArkTS)](drm-mediakeysystem-management.md)
  - [Media Key Session Management (ArkTS)](drm-mediakeysession-management.md)
- DRM Development (C/C++)
  - [Media Key System Management (C/C++)](native-drm-mediakeysystem-management.md)
  - [Media Key Session Management (C/C++)](native-drm-mediakeysession-management.md)

