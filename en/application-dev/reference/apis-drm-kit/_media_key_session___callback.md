# MediaKeySession_Callback


## Overview

The MediaKeySession_Callback struct defines the callback used to listen for events such as media key expiry or change.

**Since**: 11

**Related module**: [Drm](_drm.md)


## Summary


### Member Variables

| Name| Description| 
| -------- | -------- |
| [MediaKeySession_EventCallback](_drm.md#mediakeysession_eventcallback) [eventCallback](_drm.md#eventcallback) | Defines an event callback, for example, a media key expiry event.| 
| [MediaKeySession_KeyChangeCallback](_drm.md#mediakeysession_keychangecallback) [keyChangeCallback](_drm.md#keychangecallback) | Defines the callback of the media key change event.| 
