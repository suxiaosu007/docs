# DRM_MediaKeySystemInfo


## Overview

The DRM_MediaKeySystemInfo struct defines the media key system information obtained from a media source.

**Since**: 11

**Related module**: [Drm](_drm.md)


## Summary


### Member Variables

| Name| Description| 
| -------- | -------- |
| uint32_t [psshCount](_drm.md#psshcount) | Defines the number of pieces of Protection Scheme Specific Header (PSSH) data.| 
| [DRM_PsshInfo](_d_r_m___pssh_info.md) [psshInfo](_drm.md#psshinfo) [[MAX_PSSH_INFO_COUNT](_drm.md#max_pssh_info_count)] | Defines the PSSH information.| 
