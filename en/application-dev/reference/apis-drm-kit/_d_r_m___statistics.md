# DRM_Statistics


## Overview

THe DRM_Statistics struct defines the statistics information about a media key system.

**Since**: 11

**Related module**: [Drm](_drm.md)


## Summary


### Member Variables

| Name| Description| 
| -------- | -------- |
| uint32_t [statisticsCount](_drm.md#statisticscount) | Defines the number of statistical items.| 
| char [statisticsName](_drm.md#statisticsname) [[MAX_STATISTICS_COUNT](_drm.md#max_statistics_count)][[MAX_STATISTICS_NAME_LEN](_drm.md#max_statistics_name_len)] | Defines the statistical item name set.| 
| char [statisticsDescription](_drm.md#statisticsdescription) [[MAX_STATISTICS_COUNT](_drm.md#max_statistics_count)][[MAX_STATISTICS_BUFFER_LEN](_drm.md#max_statistics_buffer_len)] | Defines the set of statistics information description.| 
