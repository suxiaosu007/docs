# ffrt_function_header_t


## Overview

The **ffrt_function_header_t** struct describes a task execution function.

**Related module**: [FFRT](_f_f_r_t.md)


## Summary


### Member Variables

| Name| Description|
| -------- | -------- |
| [ffrt_function_t](_f_f_r_t.md)[exec](_f_f_r_t.md#exec) |  |
| [ffrt_function_t](_f_f_r_t.md)[destroy](_f_f_r_t.md#destroy) |  |
| uint64_t [reserve](_f_f_r_t.md#reserve) [2] |  |
