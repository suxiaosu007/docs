# Test Kit API Reference

- ArkTS APIs
   - [@ohos.app.ability.abilityDelegatorRegistry (AbilityDelegatorRegistry)](js-apis-app-ability-abilityDelegatorRegistry.md)
   - [@ohos.application.testRunner (TestRunner)](js-apis-application-testRunner.md)
   - [@ohos.UiTest](js-apis-uitest.md)         
- Error Codes
   - [UiTest Error Codes](errorcode-uitest.md)
