# User Authentication Kit API Reference

- ArkTS APIs
  - [@ohos.userIAM.userAuth (User Authentication)](js-apis-useriam-userauth.md)
  - [@ohos.userIAM.faceAuth (Facial Authentication) (System API)](js-apis-useriam-faceauth-sys.md)
  - [@ohos.userIAM.userAuth (User Authentication) (System API)](js-apis-useriam-userauth-sys.md)
- Error Codes
  - [User Authentication Error Codes](errorcode-useriam.md)
