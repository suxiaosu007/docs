# native_interface_bundle.h


## Overview

The **native_interface_bundle.h** file declares the APIs for querying application information.

**Since**

9

**Related Modules**

[bundle](_bundle.md)


## Summary


### Structs

| Name| Description|
| -------- | -------- |
| [OH_NativeBundle_ApplicationInfo](_o_h___native_bundle_application_info.md) | Defines the application information.|


### Functions

| Name| Description|
| -------- | -------- |
| [OH_NativeBundle_GetCurrentApplicationInfo](_bundle.md#oh_nativebundle_getcurrentapplicationinfo)| Obtains the information about the current application.|
| [OH_NativeBundle_GetAppId](_bundle.md#oh_nativebundle_getappid) | Obtains the appId information about the current application.|
| [OH_NativeBundle_GetAppIdentifier](_bundle.md#oh_nativebundle_getappidentifier) | Obtains the appIdentifier information about the current application.|
