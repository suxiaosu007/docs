# Device Certificate Kit API Reference

- ArkTS APIs
  - [@ohos.security.cert (Certificate)](js-apis-cert.md)
  - [@ohos.security.certManager (Certificate Management)](js-apis-certManager.md)
  - [@ohos.security.certManager (Certificate Management) (System API)](js-apis-certManager-sys.md)
- Error Codes
  - [Certificate Error Codes](errorcode-cert.md)
  - [Certificate Management Error Codes](errorcode-certManager.md)
