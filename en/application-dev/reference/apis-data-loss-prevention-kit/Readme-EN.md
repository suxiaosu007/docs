# Data Loss Prevention Kit API Reference

- ArkTS APIs
  - [@ohos.dlpPermission (DLP)](js-apis-dlppermission.md)
  - [@ohos.dlpPermission (DLP) (System API)] (js-apis-dlppermission-sys.md)
- Error Codes
  - [DLP Service Error Codes](errorcode-dlp.md)
