# drawing_round_rect.h


## Overview

The **drawing_round_rect.h** file declares the functions related to the rounded rectangle in the drawing module.

**File to include**: &lt;native_drawing/drawing_round_rect.h&gt;

**Library**: libnative_drawing.so

**Since**: 11

**Related module**: [Drawing](_drawing.md)


## Summary


### Functions

| Name| Description|
| -------- | -------- |
| [OH_Drawing_RoundRect](_drawing.md#oh_drawing_roundrect) \* [OH_Drawing_RoundRectCreate](_drawing.md#oh_drawing_roundrectcreate) (const [OH_Drawing_Rect](_drawing.md#oh_drawing_rect) \*, float xRad, float yRad) | Creates an **OH_Drawing_RoundRect** object.|
| void [OH_Drawing_RoundRectDestroy](_drawing.md#oh_drawing_roundrectdestroy) ([OH_Drawing_RoundRect](_drawing.md#oh_drawing_roundrect) \*) | Destroys an **OH_Drawing_RoundRect** object and reclaims the memory occupied by the object.|
