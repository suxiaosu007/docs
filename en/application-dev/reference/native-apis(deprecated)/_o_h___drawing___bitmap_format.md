# OH_Drawing_BitmapFormat


## Overview

The **OH_Drawing_BitmapFormat** struct defines the pixel format of a bitmap, including the color type and alpha type.

**Since**: 8

**Related module**: [Drawing](_drawing.md)


## Summary


### Member Variables

| Name| Description|
| -------- | -------- |
| [OH_Drawing_ColorFormat](_drawing.md#oh_drawing_colorformat)[colorFormat](#colorformat) | Describes the storage format of bitmap pixels.|
| [OH_Drawing_AlphaFormat](_drawing.md#oh_drawing_alphaformat)[alphaFormat](#alphaformat) | Describes the alpha format of bitmap pixels.|


## Member Variable Description


### alphaFormat

```
OH_Drawing_AlphaFormat OH_Drawing_BitmapFormat::alphaFormat
```

**Description**

Describes the alpha format of bitmap pixels.


### colorFormat

```
OH_Drawing_ColorFormat OH_Drawing_BitmapFormat::colorFormat
```

**Description**

Describes the storage format of bitmap pixels.
