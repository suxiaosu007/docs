# Hid_Device


## Overview

Defines basic device information.

**Since**: 11

**Related module**: [HidDdk](_hid_ddk.md)


## Summary


### Member Variables

| Name| Description| 
| -------- | -------- |
| [deviceName](_hid_ddk.md#devicename) | const char \* | 
| [vendorId](_hid_ddk.md#vendorid) | uint16_t | 
| [productId](_hid_ddk.md#productid) | uint16_t | 
| [version](_hid_ddk.md#version) | uint16_t | 
| [bustype](_hid_ddk.md#bustype) | uint16_t | 
| [properties](_hid_ddk.md#properties) | [Hid_DeviceProp](_hid_ddk.md#hid_deviceprop) \* | 
| [propLength](_hid_ddk.md#proplength) | uint16_t | 
