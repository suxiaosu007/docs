# Hid_MscEventArray


## Overview

Defines an array of miscellaneous events.

**Since**: 11

**Related module**: [HidDdk](_hid_ddk.md)


## Summary


### Member Variables

| Name| Description|
| -------- | -------- |
| [hidMscEvent](_hid_ddk.md#hidmscevent) | [Hid_MscEvent](_hid_ddk.md#hid_mscevent) \* |
| [length](_hid_ddk.md#length-55) | uint16_t |
