# Hid_KeyCodeArray


## Overview

Defines an array of key codes.

**Since**: 11

**Related module**: [HidDdk](_hid_ddk.md)


## Summary


### Member Variables

| Name| Description| 
| -------- | -------- |
| [hidKeyCode](_hid_ddk.md#hidkeycode) | [Hid_KeyCode](_hid_ddk.md#hid_keycode) \* | 
| [length](_hid_ddk.md#length-25) | uint16_t | 
