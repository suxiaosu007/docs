# CameraManager_Callbacks


## Overview

The **CameraManager_Callbacks** struct defines the callbacks used to listen for camera status change events.

**See**

[OH_CameraManager_RegisterCallback](_o_h___camera.md#oh_cameramanager_registercallback)

**Since**: 11

**Related module**: [OH_Camera](_o_h___camera.md)


## Summary


### Member Variables

| Name| Description|
| -------- | -------- |
| [onCameraStatus](#oncamerastatus) | Camera status change event.|


## Member Variable Description


### onCameraStatus

```
OH_CameraManager_StatusCallback CameraManager_Callbacks::onCameraStatus
```

**Description**

Camera status change event.
