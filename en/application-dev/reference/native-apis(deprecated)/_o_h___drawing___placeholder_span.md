# OH_Drawing_PlaceholderSpan


## Overview

The **OH_Drawing_PlaceholderSpan** struct describes a placeholder that acts as a span.

**Since**: 11

**Related module**: [Drawing](_drawing.md)


## Summary


### Member Variables

| Name| Description|
| -------- | -------- |
| double [width](#width) | Describes the width of a placeholder.|
| double [height](#height) | Describes the height of a placeholder.|
| [OH_Drawing_PlaceholderVerticalAlignment](_drawing.md#oh_drawing_placeholderverticalalignment)[alignment](#alignment) | Describes the alignment mode of a placeholder.|
| [OH_Drawing_TextBaseline](_drawing.md#oh_drawing_textbaseline)[baseline](#baseline) | Describes the baseline of a placeholder.|
| double [baselineOffset](#baselineoffset) | Describes the baseline offset of a placeholder.|


## Member Variable Description


### alignment

```
OH_Drawing_PlaceholderVerticalAlignment OH_Drawing_PlaceholderSpan::alignment
```

**Description**

Describes the alignment mode of a placeholder.


### baseline

```
OH_Drawing_TextBaseline OH_Drawing_PlaceholderSpan::baseline
```

**Description**

Describes the baseline of a placeholder.


### baselineOffset

```
double OH_Drawing_PlaceholderSpan::baselineOffset
```

**Description**

Describes the baseline offset of a placeholder.


### height

```
double OH_Drawing_PlaceholderSpan::height
```

**Description**

Describes the height of a placeholder.


### width

```
double OH_Drawing_PlaceholderSpan::width
```

**Description**

Describes the width of a placeholder.
