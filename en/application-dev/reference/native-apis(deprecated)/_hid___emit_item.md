# Hid_EmitItem


## Overview

Defines event information.

**Since**: 11

**Related module**: [HidDdk](_hid_ddk.md)


## Summary


### Member Variables

| Name| Description| 
| -------- | -------- |
| [type](_hid_ddk.md#type) | uint16_t | 
| [code](_hid_ddk.md#code) | uint16_t | 
| [value](_hid_ddk.md#value) | uint32_t | 
