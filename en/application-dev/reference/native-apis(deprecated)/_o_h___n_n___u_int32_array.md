# OH_NN_UInt32Array


## Overview

Defines the structure for storing 32-bit unsigned integer arrays.

**Since**: 9

**Related module**: [NeuralNetworkRuntime](_neural_network_runtime.md)


## Summary


### Member Variables

| Name| Description|
| -------- | -------- |
| [data](#data) | Pointer to the unsigned integer array.|
| [size](#size) | Array length.|


## Member Variable Description


### data

```
uint32_t* OH_NN_UInt32Array::data
```

**Description**

Pointer to the unsigned integer array.


### size

```
uint32_t OH_NN_UInt32Array::size
```

**Description**

Array length.
