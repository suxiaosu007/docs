# Camera_FrameRateRange


## Overview

The **Camera_FrameRateRange** struct defines the frame rate range.

**Since**: 11

**Related module**: [OH_Camera](_o_h___camera.md)


## Summary


### Member Variables

| Name| Description| 
| -------- | -------- |
| [min](#min) | Minimum frame rate.| 
| [max](#max) | Maximum frame rate.| 


## Member Variable Description


### max

```
uint32_t Camera_FrameRateRange::max
```

**Description**

Maximum frame rate.


### min

```
uint32_t Camera_FrameRateRange::min
```

**Description**

Minimum frame rate.
