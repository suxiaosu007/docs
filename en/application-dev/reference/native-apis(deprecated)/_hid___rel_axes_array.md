# Hid_RelAxesArray


## Overview

Defines an array of relative coordinates.

**Since**: 11

**Related module**: [HidDdk](_hid_ddk.md)


## Summary


### Member Variables

| Name| Description|
| -------- | -------- |
| [hidRelAxes](_hid_ddk.md#hidrelaxes) | [Hid_RelAxes](_hid_ddk.md#hid_relaxes) \* |
| [length](_hid_ddk.md#length-45) | uint16_t |
