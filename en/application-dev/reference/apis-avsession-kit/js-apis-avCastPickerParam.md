# @ohos.multimedia.avCastPickerParam (AVCastPicker Parameters)

**avCastPickerParam** defines the enumerated values of the [@ohos.multimedia.avCastPicker](ohos-multimedia-avcastpicker.md) states.

> **NOTE**
>
> The initial APIs of this module are supported since API version 11. Newly added APIs will be marked with a superscript to indicate their earliest API version.

## AVCastPickerState

Enumerates the states of the **AVCastPicker** component.

**System capability**: SystemCapability.Multimedia.AVSession.AVCast

| Name                       | Value  | Description        |
| --------------------------- | ---- | ----------- |
| STATE_APPEARING    | 0    | The component is displayed.|
| STATE_DISAPPEARING    | 1    | The component disappears.|
