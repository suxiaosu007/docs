# AVSession Kit API Reference

- ArkTS APIs
  - [@ohos.multimedia.avsession (AVSession Management)](js-apis-avsession.md)
  - [@ohos.multimedia.avCastPickerParam (AVCastPicker Parameters)](js-apis-avCastPickerParam.md)
  - [@ohos.app.ability.MediaControlExtensionAbility (ExtensionAbility for Media Playback Control) (System API)](js-apis-app-ability-MediaControlExtensionAbility-sys.md)
  - [@ohos.multimedia.avsession (AVSession Management) (System API)](js-apis-avsession-sys.md)
  - application
    - [MediaControlExtensionContext (ExtensionAbility Context for Media Playback Control) (System API)](js-apis-inner-application-MediaControlExtensionContext-sys.md)
- ArkTS Components
  - [@ohos.multimedia.avCastPicker (AVCastPicker)](ohos-multimedia-avcastpicker.md)
- Error Codes
  - [AVSession Management Error Codes](errorcode-avsession.md)
