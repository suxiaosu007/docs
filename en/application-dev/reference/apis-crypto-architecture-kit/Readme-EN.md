# Crypto Architecture Kit API Reference

- ArkTS APIs
  - [@ohos.security.cryptoFramework (Crypto Framework)](js-apis-cryptoFramework.md)
  - APIs No Longer Maintained
    - [@system.cipher (Cipher Algorithm)](js-apis-system-cipher.md)
- Error Codes
  - [Crypto Framework Error Codes](errorcode-crypto-framework.md)
