# CalendarManager Error Codes

> **NOTE**
>
> This module involves only universal error codes. For details, see [Universal Error Codes](../errorcode-universal.md).
