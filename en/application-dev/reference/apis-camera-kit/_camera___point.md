# Camera_Point


## Overview

The **Camera_Point** struct defines the parameters that describe a point.

**Since**: 11

**Related module**: [OH_Camera](_o_h___camera.md)


## Summary


### Member Variables

| Name| Description| 
| -------- | -------- |
| double [x](#x) | X coordinate. | 
| double [y](#y) | Y coordinate. | 


## Member Variable Description


### x

```
double Camera_Point::x
```
**Description**

X coordinate.


### y

```
double Camera_Point::y
```
**Description**

Y coordinate.
