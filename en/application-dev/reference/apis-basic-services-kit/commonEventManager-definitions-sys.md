# System Common Events (System API)

This document provides indexes for system common events defined in OpenHarmony.
For details about the definition of a system common event, see [Support in @ohos.commonEventManager (Common Event)](./js-apis-commonEventManager.md#support).

  > **NOTE**
  >
  > This topic describes only system APIs provided by the module. For details about its public APIs, see [System Common Events](./commonEventManager-definitions.md).

**System capability**: SystemCapability.Notification.CommonEvent

* [COMMON_EVENT_CHARGE_TYPE_CHANGED](./common_event/commonEvent-powermgr.md#common_event_charge_type_changed)
Indicates that the system charging type has changed. This event is available only for system applications.

* [COMMON_EVENT_DEVICE_IDLE_EXEMPTION_LIST_UPDATED<sup>10+</sup> ](./common_event/commonEvent-resourceschedule.md#common_event_device_idle_exemption_list_updated)
Indicates that the exemption list for resource usage restrictions has been updated in idle mode. This event is for system applications only.
