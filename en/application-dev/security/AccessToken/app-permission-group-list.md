# Application Permission Groups


## NOTE

- It will be helpful if you understand [permission groups and permissions](app-permission-mgmt-overview.md#permission-groups-and-permissions) before requesting permission groups for your application.

- The following lists the permission groups supported by the system. For details about the permissions, see [Permissions for All Applications](permissions-for-all.md).


## Location

- ohos.permission.LOCATION_IN_BACKGROUND

- ohos.permission.LOCATION

- ohos.permission.APPROXIMATELY_LOCATION


## Camera

- ohos.permission.CAMERA


## Microphone

- ohos.permission.MICROPHONE


## Calendar

- ohos.permission.READ_CALENDAR

- ohos.permission.WRITE_CALENDAR

- ohos.permission.READ_WHOLE_CALENDAR

- ohos.permission.WRITE_WHOLE_CALENDAR


## Fitness

- ohos.permission.ACTIVITY_MOTION


## Body Sensors

- ohos.permission.READ_HEALTH_DATA


## Multi-device Collaboration

- ohos.permission.DISTRIBUTED_DATASYNC


## Phone

- ohos.permission.ANSWER_CALL

- ohos.permission.MANAGE_VOICEMAIL


## Call Logs

- ohos.permission.READ_CALL_LOG

- ohos.permission.WRITE_CALL_LOG


## Messaging

- ohos.permission.READ_CELL_MESSAGES

- ohos.permission.READ_MESSAGES

- ohos.permission.RECEIVE_MMS

- ohos.permission.RECEIVE_SMS

- ohos.permission.RECEIVE_WAP_MESSAGES

- ohos.permission.SEND_MESSAGES


## Music and Audio

- ohos.permission.WRITE_AUDIO

- ohos.permission.READ_AUDIO


## Media and Files

- ohos.permission.READ_DOCUMENT

- ohos.permission.WRITE_DOCUMENT

- ohos.permission.READ_MEDIA

- ohos.permission.WRITE_MEDIA


## Images and Videos

- ohos.permission.WRITE_IMAGEVIDEO

- ohos.permission.READ_IMAGEVIDEO

- ohos.permission.MEDIA_LOCATION


## Ad Tracking

- ohos.permission.APP_TRACKING_CONSENT


## Installed Bundle List

- ohos.permission.GET_INSTALLED_BUNDLE_LIST


## Bluetooth

- ohos.permission.ACCESS_BLUETOOTH
