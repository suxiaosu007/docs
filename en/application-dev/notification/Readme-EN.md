# Notification

- [Introduction to Notification Kit](notification-overview.md)
- [Requesting Notification Authorization](notification-enable.md)
- [Managing the Notification Badge](notification-badge.md)
- [Managing Notification Slots](notification-slot.md)
- Publishing a Notification
  - [Publishing a Text Notification](text-notification.md)
  - [Publishing a Progress Notification](progress-bar-notification.md)
  - [Publishing a Live View Notification (for System Applications Only)](live-view-notification.md)
  - [Adding a WantAgent Object to a Notification](notification-with-wantagent.md)
- [Subscribing to Notifications (for System Applications Only)](notification-subscription.md)